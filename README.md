# Master Thesis Sunniva Flück

## Description
In the course of the master thesis a testing tool was developed. This can be found in the folder 'OAITT'.

Additionally necessary code changes for setting up the O-RAN platform OSC are located in the folder 'code_changes_osc_setup'.

The full thesis with more information can be found in [Master Thesis](https://gitlab.ethz.ch/fluecks/master_thesis/-/raw/main/Master_Thesis_Sunniva_Flu%CC%88ck.pdf)

