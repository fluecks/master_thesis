/*
Coded By Leeon123
Date: 12/5/2019
|------------------------------------------------|
|  This tool is a server stress test tool,       |
|  It is only use for testing server firewall    |
|  and education.                                |
|------------------------------------------------|
Updated: 6/8/2020
*/
package main

import (
	"crypto/tls"
	"fmt"
	//"io/ioutil"
	"math/rand"
	"net"
	"os"
	"strings"
	"strconv"
	"sync"
	"time"
)

//Start of Random useragent
var (
	str       string = "asdfghjklqwertyuiopzxcvbnmASDFGHJKLQWERTYUIOPZXCVBNM=&"
	acceptall        = []string{
		"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,/;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\n",
		"Accept-Encoding: gzip, deflate\r\n",
		"Accept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\n",
		"Accept: text/html, application/xhtml+xml, application/xml;q=0.9, /;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Charset: iso-8859-1\r\nAccept-Encoding: gzip\r\n",
		"Accept: application/xml,application/xhtml+xml,text/html;q=0.9, text/plain;q=0.8,image/png,/;q=0.5\r\nAccept-Charset: iso-8859-1\r\n",
		"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,/;q=0.8\r\nAccept-Encoding: br;q=1.0, gzip;q=0.8, ;q=0.1\r\nAccept-Language: utf-8, iso-8859-1;q=0.5, ;q=0.1\r\nAccept-Charset: utf-8, iso-8859-1;q=0.5\r\n",
		"Accept: image/jpeg, application/x-ms-application, image/gif, application/xaml+xml, image/pjpeg, application/x-ms-xbap, application/x-shockwave-flash, application/msword, /\r\nAccept-Language: en-US,en;q=0.5\r\n",
		"Accept: text/html, application/xhtml+xml, image/jxr, /\r\nAccept-Encoding: gzip\r\nAccept-Charset: utf-8, iso-8859-1;q=0.5\r\nAccept-Language: utf-8, iso-8859-1;q=0.5, ;q=0.1\r\n",
		"Accept: text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, /;q=0.1\r\nAccept-Encoding: gzip\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Charset: utf-8, iso-8859-1;q=0.5\r\n",
		"Accept: text/html, application/xhtml+xml, application/xml;q=0.9, /;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\n",
		"Accept-Charset: utf-8, iso-8859-1;q=0.5\r\nAccept-Language: utf-8, iso-8859-1;q=0.5, ;q=0.1\r\n",
		"Accept: text/html, application/xhtml+xml",
		"Accept-Language: en-US,en;q=0.5\r\n",
		"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,/;q=0.8\r\nAccept-Encoding: br;q=1.0, gzip;q=0.8, ;q=0.1\r\n",
		"Accept: text/plain;q=0.8,image/png,/;q=0.5\r\nAccept-Charset: iso-8859-1\r\n"}
	start    = make(chan bool)
	referers = []string{
		"https://www.google.com/search?q=",
		"https://check-host.net/",
		"https://www.facebook.com/",
		"https://www.youtube.com/",
		"https://www.fbi.com/",
		"https://www.bing.com/search?q=",
		"https://r.search.yahoo.com/",
		"https://www.cia.gov/index.html",
		"https://www.police.gov.hk/",
		"https://www.mjib.gov.tw/",
		"https://www.president.gov.tw/",
		"https://www.gov.hk",
		"https://vk.com/profile.php?auto=",
		"https://www.usatoday.com/search/results?q=",
		"https://help.baidu.com/searchResult?keywords=",
		"https://steamcommunity.com/market/search?q=",
		"https://www.ted.com/search?q=",
		"https://play.google.com/store/search?q=",
	}
	choice  = []string{"Macintosh", "Windows", "X11"}
	choice2 = []string{"68K", "PPC", "Intel Mac OS X"}
	choice3 = []string{"Win3.11", "WinNT3.51", "WinNT4.0", "Windows NT 5.0", "Windows NT 5.1", "Windows NT 5.2", "Windows NT 6.0", "Windows NT 6.1", "Windows NT 6.2", "Win 9x 4.90", "WindowsCE", "Windows XP", "Windows 7", "Windows 8", "Windows NT 10.0; Win64; x64"}
	choice4 = []string{"Linux i686", "Linux x86_64"}
	choice5 = []string{"chrome", "spider", "ie"}
	choice6 = []string{".NET CLR", "SV1", "Tablet PC", "Win64; IA64", "Win64; x64", "WOW64"}
	spider  = []string{
		"AdsBot-Google ( http://www.google.com/adsbot.html)",
		"Baiduspider ( http://www.baidu.com/search/spider.htm)",
		"FeedFetcher-Google; ( http://www.google.com/feedfetcher.html)",
		"Googlebot/2.1 ( http://www.googlebot.com/bot.html)",
		"Googlebot-Image/1.0",
		"Googlebot-News",
		"Googlebot-Video/1.0",
	}
)

func useragent() string {
	platform := choice[rand.Intn(len(choice))]
	var os string
	if platform == "Macintosh" {
		os = choice2[rand.Intn(len(choice2)-1)]
	} else if platform == "Windows" {
		os = choice3[rand.Intn(len(choice3)-1)]
	} else if platform == "X11" {
		os = choice4[rand.Intn(len(choice4)-1)]
	}
	browser := choice5[rand.Intn(len(choice5)-1)]
	if browser == "chrome" {
		webkit := strconv.Itoa(rand.Intn(599-500) + 500)
		uwu := strconv.Itoa(rand.Intn(99)) + ".0" + strconv.Itoa(rand.Intn(9999)) + "." + strconv.Itoa(rand.Intn(999))
		return "Mozilla/5.0 (" + os + ") AppleWebKit/" + webkit + ".0 (KHTML, like Gecko) Chrome/" + uwu + " Safari/" + webkit
	} else if browser == "ie" {
		uwu := strconv.Itoa(rand.Intn(99)) + ".0"
		engine := strconv.Itoa(rand.Intn(99)) + ".0"
		option := rand.Intn(1)
		var token string
		if option == 1 {
			token = choice6[rand.Intn(len(choice6)-1)] + "; "
		} else {
			token = ""
		}
		return "Mozilla/5.0 (compatible; MSIE " + uwu + "; " + os + "; " + token + "Trident/" + engine + ")"
	}
	return spider[rand.Intn(len(spider))]
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	fmt.Println("|--------------------------------------|")
	fmt.Println("|   Golang : Server Stress Test Tool   |")
	fmt.Println("|          C0d3d By Lee0n123           |")
	fmt.Println("|--------------------------------------|")
	if len(os.Args) < 7 {
		fmt.Printf("Usage: %s host port mode connections seconds timeout(second)\r\n", os.Args[0])
		fmt.Println("|--------------------------------------|")
		fmt.Println("|             Mode List                |")
		fmt.Println("|     [1] TCP-Connection flood         |")
		fmt.Println("|     [2] HTTP-GET flood (Auto SSL)    |")
		fmt.Println("|     [3] HTTP-PUT-flood (Auto SSL)    |")
		fmt.Println("|--------------------------------------|")
		os.Exit(1)
	}
	var mutex sync.Mutex
	count := 0
	succ := 0
	toolate := 0
	late := 0
	sent := 0
	senderror := 0
	stop := 0
	stopsending := 0 //stop
	stopreceiving := 0

	errn := 0
	connections, err := strconv.Atoi(os.Args[4])
	if err != nil {
		fmt.Println("connections should be an integer")
		return
	}
	times, err := strconv.Atoi(os.Args[5])
	if err != nil {
		fmt.Println("seconds should be an integer")
		return
	}
	timeout, err := strconv.Atoi(os.Args[6])
	if err != nil {
		fmt.Println("timeout should be a integer")
		return
	}
	entrypoint := os.Args[7]
	policy_type := os.Args[8]
	//policy_path := os.Args[9]
	policy := os.Args[9]
	addr := os.Args[1]
	addr += ":"
	addr += os.Args[2]
	receivingtimeout := 90
	var wg sync.WaitGroup
	if os.Args[3] == "1" { //Tcp connection flood
		payload := "\000"
		for i := 0; i < connections; i++ {
			wg.Add(1)
			go func(wg *sync.WaitGroup) {
				defer wg.Done()
				s, err := net.DialTimeout("tcp", addr, time.Duration(timeout)*time.Second)
				if err != nil {
					errn++
					fmt.Println("Error:", err)
					return
				}
				if s, ok := s.(*net.TCPConn); ok {
					s.SetNoDelay(false)
				}
				err = s.(*net.TCPConn).SetKeepAlive(true)
				if err != nil {
					errn++
					fmt.Println("Error:", err)
					return
				}
				for {
					if stop > 0 {
						_, err := s.Write([]byte(payload)) //check if it still alive
						if err != nil {
							errn++
							fmt.Println("Error:", err)
						} else {
							count++
						}
						break
					} else {
						time.Sleep(time.Millisecond * 100)
					}
				}

			}(&wg)
		}
		time.Sleep(time.Second * time.Duration(times)) //timer
		stop++
		wg.Wait()
		fmt.Println("Number of Threads: ", connections)
		fmt.Println("URL: ", addr)
		fmt.Println("Given Runtime", times)
		fmt.Println("Timeout: ", timeout)
		fmt.Println("Total connections:", connections)
		fmt.Println("Connection Alive:", count+1)
		fmt.Println("Connection Error:", errn, "times")
	} else if os.Args[3] == "2" { //http/s get flood
		successful_durations := time.Duration(0)
		durations := time.Duration(0)
		payload := " HTTP/1.1\r\nHost: " + os.Args[1] + "\r\nConnection: Keep-Alive\r\nUser-Agent: " + useragent() + "\r\nAccept: application/xml,application/xhtml+xml,text/html;q=0.9, text/plain;q=0.8,image/png,*/*;q=0.5\r\nAccept-Charset: iso-8859-1\r\n\r\n"
		url := "GET " + entrypoint
		warmupover := false
		warmuptime := 30
		for i := 0; i < connections; i++ {
			selfsucc := 0
			selftoolate := 0
			selflate := 0
			selfsent := 0
			selfsenderror := 0
			var receiveError error
			var duration time.Duration
			selfdurations := time.Duration(0)
			selfsuccessful_durations := time.Duration(0)
			wg.Add(1)
			go func(wg *sync.WaitGroup) {
				defer wg.Done()
				for {
					if stopsending > 0 {
						break
					}
					s, err := net.DialTimeout("tcp", addr, time.Duration(timeout)*time.Second)
					if err != nil {
						fmt.Println(err)
						errn++
						return
					}
					if os.Args[2] == "443" {
						s = tls.Client(s, &tls.Config{
							ServerName: addr, InsecureSkipVerify: true,
						})
					}
					if s, ok := s.(*net.TCPConn); ok {
						s.SetNoDelay(false)
					}
					
					
					for t := 0; t < 140; t++ {
						s.SetDeadline(time.Now().Add((time.Duration(warmuptime) + time.Duration(times) + time.Duration(receivingtimeout)) * time.Second))
						tmp := make([]byte, 4162)  
						startTime := time.Now()   
						if warmupover{
							selfsent++ 
						}
						s.Write([]byte(url + payload))
						var n int
						var err error

						for {
							n, err = s.Read(tmp)
							if n < 4096{
								break
							}
						}
						
						endTime := time.Now()
						if stopsending > 0 {
							receiveError = err
							break
						}
						if warmupover{
							duration = endTime.Sub(startTime)
							if err != nil{
								selfsenderror++
							} else {
								selfsucc++
								selfsuccessful_durations += duration
								selfdurations += duration
							}
						}
					}
					s.Close()	
				}
				if stopreceiving == 0 {
					if err != nil{
						selfsenderror++
					} else{
						selfdurations += duration
						selflate = 1
					}
				} else {
					selftoolate++
				}
				mutex.Lock()
				defer mutex.Unlock()
				succ += selfsucc
				toolate += selftoolate
				late += selflate
				senderror += selfsenderror
				sent += selfsent
				durations += selfdurations
				successful_durations += selfsuccessful_durations
			}(&wg)
			
			
		}
		time.Sleep(time.Second * time.Duration(warmuptime))
		warmupover = true
		time.Sleep(time.Second * time.Duration(times)) //timer
		stopsending++
		time.Sleep(time.Second * time.Duration(receivingtimeout))
		stopreceiving++
		
		wg.Wait()
		fmt.Println("Number of Threads: ", connections)
		fmt.Println("URL: ", addr, entrypoint)
		fmt.Println("Warmup Time: ", warmuptime)
		fmt.Println("Given Runtime:", times)
		fmt.Println("Timeout: ", 2*times + warmuptime)
		fmt.Println()
		fmt.Println("Total Sent:", sent, "requests")
		fmt.Printf("Sent RPS: %.2f requests/s\r\n", float64(sent)/float64(times))
		fmt.Println()
		fmt.Printf("Received RPS: %.2f requests/s\r\n", float64(succ)/float64(times))
		fmt.Printf("Response Arrived during Sending Time: %d requests \r\n", succ)
		fmt.Printf("Average Response Time of Succeeded Requests During Runtime: %.5f \r\n", (successful_durations/time.Duration(succ)).Seconds())
		fmt.Println()
		fmt.Printf("Response Arrived after Sending Time: %d requests \r\n", late)
		fmt.Printf("Response Did not Arrive within Deadline: %d requests \r\n", toolate)
		fmt.Printf("Average Response Time of all Requests: %.5f \r\n", (durations/time.Duration(succ + late)).Seconds())
		fmt.Println()
		fmt.Printf("Error while sending: %d requests \r\n", senderror)
		fmt.Println("Connection Error:", errn, "times")
	} else if os.Args[3] == "3" { //http/s put flood
		policy_test := strings.Replace(policy, "\n", "", -1)
		counter := 1
		successful_durations := time.Duration(0)
		durations := time.Duration(0)
		payload := " HTTP/1.1\r\nHost: " + os.Args[1] + "\r\nConnection: Keep-Alive\r\nUser-Agent: " + useragent() + "\r\nAccept: */*\r\nContent-Type: application/json\r\nContent-Length: 359\r\n\r\n" + policy_test
		url := "PUT " + entrypoint + "policytypes/" + policy_type + "/policies/"
		warmupover := false
		warmuptime := 30
		for i := 0; i < connections; i++ {
			selfsucc := 0
			selftoolate := 0
			selflate := 0
			selfsent := 0
			selfsenderror := 0
			var receiveError error
			var duration time.Duration
			selfdurations := time.Duration(0)
			selfsuccessful_durations := time.Duration(0)
			wg.Add(1)
			go func(wg *sync.WaitGroup) {
				defer wg.Done()
				for {
					if stopsending > 0 {
						break
					}
					s, err := net.DialTimeout("tcp", addr, time.Duration(timeout)*time.Second)
					if err != nil {
						errn++
						fmt.Println(err)
						return
					}
					if os.Args[2] == "443" {
						s = tls.Client(s, &tls.Config{
							ServerName: addr, InsecureSkipVerify: true,
						})
					}
					if s, ok := s.(*net.TCPConn); ok {
						s.SetNoDelay(false)
					}
					
				
					for t := 0; t < 140; t++ {
						s.SetDeadline(time.Now().Add((time.Duration(warmuptime) + time.Duration(times) + time.Duration(receivingtimeout)) * time.Second))
						tmp := make([]byte, 256)  
						startTime := time.Now() 
						if warmupover{  
							selfsent++ 
						}                                                                                                                                                                                                                                                                   // using small tmo buffer for demonstrating
						s.Write([]byte(url + strconv.Itoa(counter) + payload))
						var n int
						var err error
						for {
							n, err = s.Read(tmp)
							if n < 4096{
								break
							}
						}
						endTime := time.Now()
						counter++
						if stopsending > 0 {
							receiveError = err
							break
						}
						if warmupover{
							duration = endTime.Sub(startTime)
							if err != nil{
								selfsenderror++
							} else {
								selfsucc++
								selfsuccessful_durations += duration
								selfdurations += duration
							}
						}
					}
					s.Close()
				}
				if stopreceiving == 0 {
					if err != nil{
						selfsenderror++
					} else{
						selfdurations += duration
						selflate = 1
					}
				} else {
					selftoolate++
				}
				mutex.Lock()
				defer mutex.Unlock()
				succ += selfsucc
				toolate += selftoolate
				late += selflate
				senderror += selfsenderror
				sent += selfsent
				durations += selfdurations
				successful_durations += selfsuccessful_durations
			}(&wg)
		}
		time.Sleep(time.Second * time.Duration(warmuptime))
		warmupover = true
		time.Sleep(time.Second * time.Duration(times)) //timer
		stopsending++
		time.Sleep(time.Second * time.Duration(receivingtimeout))
		stopreceiving++
		wg.Wait()
		fmt.Println("Number of Threads: ", connections)
		fmt.Println("URL: ", addr, url)
		fmt.Println("Warmup Time: ", warmuptime)
		fmt.Println("Given Sending Time:", times, " s")
		fmt.Println("Deadline: ", 2*times + warmuptime, " s")
		fmt.Println()
		fmt.Println("Total Sent:", sent, "requests")
		fmt.Printf("Sent RPS: %.2f requests/s\r\n", float64(sent)/float64(times))
		fmt.Println()
		fmt.Printf("Received RPS: %.2f requests/s\r\n", float64(succ)/float64(times))
		fmt.Printf("Response Arrived during Sending Time: %d requests \r\n", succ)
		fmt.Printf("Average Response Time of Succeeded Requests during Sending Time: %.5f s\r\n", (successful_durations/time.Duration(succ+1)).Seconds())
		fmt.Println()
		fmt.Printf("Response arrived after Sending Time but within Deadline: %d requests \r\n", late)
		fmt.Printf("Response did not arrive within Deadline: %d requests \r\n", toolate)
		fmt.Printf("Average Response Time of all Requests: %.5f s\r\n", (durations/time.Duration(succ + late + 1)).Seconds())
		fmt.Println()
		fmt.Printf("Error while sending: %d requests \r\n", senderror)
		fmt.Println("Connection Error:", errn, "times")
	}
}
