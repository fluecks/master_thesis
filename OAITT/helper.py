import colorist
import datetime
import logging
import subprocess

class Session:
    def __init__(self):    
        self.a1_ip = None
        self.a1_port = None
        self.entrypoint = None
        self.policy_path = None
        self.policy_type = None
        self.parameter_file = None
        init_datetime = datetime.datetime.now()
        self.logdir = init_datetime.strftime('%Y_%m_%d_%H_%M_%S') + '_log.log'
        subprocess.run(['mkdir', self.logdir])
        self.tls_certificate = None
        self.summary_logger = SpecificLogger('summary', self)


class CountHandler(logging.Handler):
    def __init__(self, message_type: int = None):
        super().__init__()
        self.records = []
        self.message_type = message_type

    def emit(self, record):
        if record.levelno == self.message_type:
            self.records.append(record.getMessage())
                   

class SpecificLogger:
    def __init__(self, method, session):
        self.method = method
        self.session = session
        # only configure new logger if it does not already exist
        if method not in logging.Logger.manager.loggerDict.keys():
            self.logger = logging.getLogger(method)
            self.logger.setLevel(logging.DEBUG)
            current_datetime = datetime.datetime.now()
            logfile = logging.FileHandler(session.logdir + '/' + method + '.log')
            logfile.setLevel(logging.INFO)
            formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
            logfile.setFormatter(formatter)
            self.logger.addHandler(logfile)
        else:
            self.logger = logging.getLogger(method)    
            
        self.error_count_handler = CountHandler(40)
        self.logger.addHandler(self.error_count_handler)
        self.warning_count_handler = CountHandler(30)
        self.logger.addHandler(self.warning_count_handler)

            
    def summary(self):
        self.session.summary_logger.logger.info('Summary of: %s', self.method)
        print('--------------------------------------------------------- \nRESULT:\n---------------------------------------------------------')
        self.session.summary_logger.logger.info('---------------------------------------------------------')
        self.session.summary_logger.logger.info('RESULT:')
        self.session.summary_logger.logger.info('---------------------------------------------------------')
        if len(self.error_count_handler.records) == 0:
            colorist.green('No ERRORS')
            self.session.summary_logger.logger.info('No Errors')
        else:
            colorist.red('ERRORS:')
            self.session.summary_logger.logger.info('ERRORS:')
            for record in self.error_count_handler.records:
                print(record)  
                self.summary_logger.logger.info(record)   
        if len(self.warning_count_handler.records) == 0:
            colorist.green('No VIOLATIONS according to the O-RAN specifications') 
            self.session.summary_logger.logger.info('No VIOLATIONS according to the O-RAN specifications')
        else:
            colorist.yellow('\nVIOLATIONS according to the O-RAN specifications:') 
            self.session.summary_logger.logger.info('VIOLATIONS according to the O-RAN specifications:')     
            for record in self.warning_count_handler.records:
                print(record)  
                self.session.summary_logger.logger.info(record)
        print('\nFor more information check the full log.\n---------------------------------------------------------')    
        self.session.summary_logger.logger.info('---------------------------------------------------------') 
        self.session.summary_logger.logger.info('---------------------------------------------------------')           