from colorist import green, red, yellow
import dos_with_stress
from helper import Session
import ipaddress
import json
import logging
import multiple_non_rt_attack
import oauth2
import os
import policy_fuzzer
import requests
import tls_tester
import url_fuzzer


def is_valid_ip_address(ip: str = None) -> bool:
    if type(ip) == str and ip != '':
        try:
            ipaddress.ip_address(ip)
            return True
        except ValueError:
            return False
    return False    
    
def is_valid_port(port: str = None) -> bool:
    if type(port) == str and port != '':
        try:
            check_int = int(port)
            return True
        except: 
            return False   
    return False  

def is_valid_entrypoint(entrypoint: str = None) -> bool:
    if entrypoint[0] == '/':
        return True  
    return False   

def is_valid_policy_type(policy_type: str = None) -> bool:
    if policy_type != '':
        return True  
    return False  

def is_valid_policy_path(policy_path: str = None) -> bool:
    if os.path.exists(policy_path):
        return True  
    return False  

def is_valid_tls_cert(cert_path: str = None) -> bool:
    if os.path.exists(cert_path):
        return True  
    return False 

def show_menu():
    print('\nWhich test do you want to run?\n')
    green('1. Oauth2')
    green('2. DoS (TCP/HTTP/Policy)')
    green('3. TLS Tests')
    green('4. Policy Fuzzing')
    green('5. URL Fuzzing')
    green('6. Multiple Non-RT RIC')
    yellow('7. Exit')
    
def get_user_choice() -> str:
    choice = input('\nEnter your choice (number): ')
    return choice

def input_type_print():
    print('\nHow do you want to provide the input parameters?')
    print('1. Parameter File')
    print('2. Manual input in command line')
    
def read_input_file(session):
    correct = False
    while not correct:
        session.parameter_file = input('Path to parameter file:')
        try:
            f = open(session.parameter_file)
            data = json.load(f) 
            session.a1_ip = data['a1_ip']
            session.a1_port = data['a1_port']
            session.entrypoint = data['entrypoint']
            session.policy_type = data['policy_type']
            session.policy_path = data['policy_path']
            correct = True
            if 'tls_certificate' in data:
                session.tls_certificate = data['tls_certificate'] 
                correct = is_valid_tls_cert(session.tls_certificate)               
            correct = correct and is_valid_ip_address(session.a1_ip) and is_valid_port(session.a1_port) and is_valid_entrypoint(session.entrypoint) and is_valid_policy_type(session.policy_type) and is_valid_policy_path(session.policy_path)
            if not correct:
                print('One of the provided parameters is not valid.')
        except Exception as e:
            print('Input file could not be loaded. Try again.') 
            print(e)
                  
def check_input_file(session: Session):
    while True:
        input_type_print()
        choice = get_user_choice()
        if choice == '1':
            read_input_file(session)
            return
        if choice == '2':
            return        

def get_a1_ip() -> str:
    while True:
        a1_ip = input('Enter A1 interface IP: ')
        if is_valid_ip_address(a1_ip):
            return a1_ip
        else:
            print('Not a valid IP address. Try again.')

def get_a1_port() -> str:
    while True:
        a1_port = input('Enter A1 interface port: ')
        if is_valid_port(a1_port):
            return a1_port
        else:
            print('Not a valid port. Try again.')

def get_entrypoint() -> str:
    while True:
        entrypoint = input('Enter the entrypoint to the Policy Server e.g. (/, /a1-p/...): ')
        if is_valid_entrypoint(entrypoint):
            return entrypoint
        else:
            print('Entrypoint url has to start with a "/". Try again.')

def get_policy_type() -> str:
    while True:
        policy_type = input('Enter the policytype that is registered on the policy server: ')
        if is_valid_policy_type(policy_type):
            return policy_type
        else:
            print('Policy type can not be None. Try again.')

def get_policy_path() -> str:
    while True:
        policy_path = input('Enter the path to the policy file: ')
        if is_valid_policy_path(policy_path):
            return policy_path
        else:
            print('Path does not exist. Try again.')
 
def get_tls_cert_path() -> str:
    while True:
        cert_path = input('Enter the path to the tls certificate: ')
        if is_valid_tls_cert:
            return cert_path
        else:
            print('Path does not exist. Try again.')
                
def check_new_input(session: Session = None):
    new_input = True
    if session.a1_ip and session.a1_port and session.entrypoint:
        print('\nUse same input parameters?')
        same = input('y/n: ')
        if same == 'y' or same == 'Y':
            new_input = False
    if new_input:
        check_input_file(session) 
        if session.parameter_file == None:    
            session.a1_ip = get_a1_ip()
            session.a1_port = get_a1_port()
            session.entrypoint = get_entrypoint()     

def check_new_policy(session: Session = None):
    new_input = True
    if session.policy_path and session.policy_type and session.parameter_file == None:
        print('\nUse same policy?')
        same = input('y/n: ')
        if same == 'y' or same == 'Y':
            new_input = False
    if new_input and session.parameter_file == None:
        session.policy_type = get_policy_type()
        session.policy_path = get_policy_path() 
        
def check_tls(session: Session = None):
    url = 'http://' + session.a1_ip + ':' + session.a1_port + session.entrypoint + 'policytypes'
    response = requests.get(url, allow_redirects=True)
    
    final_url = response.url
    
    if final_url.startswith('https://'):
        if session.tls_certificate == None:
            session.tls_certificate = get_tls_cert_path()
    else:
        session.tls_certificate = None             
            
    
    
def main():
    session = Session()
    
    while True:
        show_menu()
        choice = get_user_choice()
        if choice == '1':
            check_new_input(session)
                
            print('\n\n')   
            print('Starting OAuth2 Check \n') 
            print('\n\n')
            check_tls(session)
            attacker = oauth2.Authenticator(session)
            attacker.run()
            print('\n\n')
            print('Finished OAuth2 Check')
            print('\n\n')  
            
        elif choice == '2':
            check_new_input(session)
            check_new_policy(session)
            
            print('\n\n')
            print('Starting Stress Test \n') 
            print('\n\n')
            check_tls(session)
            attacker = dos_with_stress.Stresser(session)
            attacker.run()
            print('\n\n')
            print('Finished Stress Test')
            print('\n\n')  
            
        elif choice == '3':
            check_new_input(session)     
            
            print('\n\n')    
            print('Starting the SSL/TLS analysis...\n')
            print('\n\n')  
            check_tls(session)
            attacker = tls_tester.TLSTester(session)
            attacker.run()
            print('\n\n')
            print('Finished SSL/TLS analysis')
            print('\n\n')
              
              
        elif choice == '4':
            check_new_input(session)
            check_new_policy(session)
                
            print('\n\n') 
            print('Starting Policy Fuzzing Test \n') 
            print('\n\n') 
            check_tls(session) 
            attacker = policy_fuzzer.PolicyFuzzer(session)
            attacker.run()
            print('\n\n')
            print('Finished Policy Fuzzing Test') 
            print('\n\n')
        
        elif choice == '5':
            check_new_input(session)
                
            print('\n\n') 
            print('Starting URL Fuzzing Test \n')  
            print('\n\n') 
            check_tls(session)
            attacker = url_fuzzer.UrlFuzzer(session)
            attacker.run()
            print('\n\n')
            print('Finished URL Fuzzing Test') 
            print('\n\n')    
                
                
        elif choice == '6':
            check_new_input(session)
            check_new_policy(session)      
                
            print('\n\n')  
            print("Starting the multiple Non-RT RIC attack...\n")
            print('\n\n')
            check_tls(session)
            attacker = multiple_non_rt_attack.MultipleNonRtAttacker(session)
            attacker.run()
            print('\n\n')
            print("Finished the multiple Non-RT RIC attack...")
            print('\n\n')  
              
                 
        elif choice == '7':
            print('\n\n')
            print('Exiting...')
            print('\n\n')
            break
        
        else:
            print('Invalid choice. Please try again.')


if __name__ == '__main__':
    main()