import ast
import concurrent.futures
import docker
from helper import SpecificLogger
import os
import requests
import subprocess
from time import sleep


class Stresser:
    def __init__(self, session):
        self.s = session
        self.log = SpecificLogger('stress_testing', session)
        self.concurrent = True
        self.counter = 0
        
    def run(self):
        self.show_menu()
        choice = self.get_attack_choice()
        os.system('go build ./Stress-tester/stress.go')
        if choice == 1:
            container = self.setup_docker()
            try:
                self.stress_testing_tcp(container)
                self.health_check()
            except:
                print("Stress tester did not work")    
            self.clean_docker()
        elif choice == 2:    
            container = self.setup_docker()
            try:
                self.stress_testing_http(container)
                self.health_check()
            except:
                print("Stress tester did not work")    
            
            self.clean_docker()  
        elif choice == 3:
            container = self.setup_docker()
            try:
                self.stress_testing_policy(container) 
                self.health_check()
            except Exception as e:
                print("Stress tester did not work: ", e)    
            self.clean_docker()  
        elif choice == 4:  
            return  
        self.log.summary()         

    def show_menu(self):
        print('1. TCP connection flood')
        print('2. HTTP GET flood')
        if self.s.policy_type:
            print('3. HTTP PUT policy flood')
            print('4. Exit')
        else:
            print('3. Exit')    
        
    def get_attack_choice(self) -> int:
        while True:
            choice = input('Enter your attack choice (number): ') 
            if self.s.policy_type: 
                if choice in ['1', '2', '3', '4']:
                    return int(choice)
            else:
                if choice in ['1', '2', '3']:  
                    if choice == '3':
                        return 4
                    return int(choice)  
            print('Not a valid choice. Try again.')  
            
    def get_starting_point(self) -> str:    
        print('Which URL do you want to use for the HTTP GET flood?')
        print('1. ' + self.s.entrypoint)
        print('2. ' + self.s.entrypoint + 'policytypes')
        if self.s.policy_type:
            print('3. ' + self.s.entrypoint + 'policytypes/' + self.s.policy_type)
            print('4. ' + self.s.entrypoint + 'policytypes/' + self.s.policy_type + '/policies')
            print('5. Own Input')
        else:
            print('3. Own Input')    
        while True:
            choice = input('Enter your choice (number): ')
            if self.s.policy_type:
                if choice in ['1', '2', '3', '4', '5']:
                    break
            else:
                if choice in ['1', '2', '3']:
                    break
            print('Not a valid choice. Try again.')  
        if choice == '1':
            return self.s.entrypoint
        elif choice == '2':
            return self.s.entrypoint + 'policytypes'
        elif choice == '3':
            if self.s.policy_type:
                return self.s.entrypoint + 'policytypes/' + self.s.policy_type 
            return  input('Enter custom URL: ')
        elif choice == '4':
            if self.s.policy_type:
                return self.s.entrypoint + 'policytypes/' + self.s.policy_type + '/policies'
        elif choice == '5':    
            return input('Enter custom URL: ')       
               
    def setup_docker(self):
        """
        Sets up the docker environment that is needed for the attack
        """
        subprocess.run(['sudo', 'apt', '-y', 'install', 'docker.io'], capture_output=True)
        subprocess.run(['sudo', 'chmod', '777', '/var/run/docker.sock'], capture_output=True)
        self.client = docker.from_env()
        self.create_network()
        container = self.new_docker_container('custom_container', '173.20.1.11') 
        self.command_in_container(container, 'apt-get update')
        self.command_in_container(container, 'apt-get -y install netcat-openbsd')  
        return container  
    
    def clean_docker(self):
        """
        Cleans up the created docker environment
        """
        subprocess.run(['sudo', 'docker', 'stop', 'custom_container'], capture_output=True)
        subprocess.run(['sudo', 'docker', 'rm', 'custom_container'], capture_output=True)
        subprocess.run(['sudo', 'docker', 'network', 'rm', 'network1'], capture_output=True)  
        
    def new_docker_container(self, name: str = 'container', ip: str = None) -> docker.models.containers.Container:
        """
        Creates new docker container
        Parameters:
            name: name of the docker container
            ip: ip of the docker container
        Returns:
            container    
        """
        # Specify the image and other container settings
        image_name = 'nginx:latest'
        container_name = name

        # Start the container
        container = self.client.containers.run(image_name, detach=True, tty=True, name=container_name)
        self.net.connect(container,ipv4_address=ip)
        self.log.logger.info('Started container %s with ID: %s and IP: %s', container_name, container.id, ip) 
        return container  
        
    def create_network(self):
        """
        Creates new docker subnet to connect docker containers to
        """
        ipam_pool = docker.types.IPAMPool(subnet='173.20.1.0/24', gateway='173.20.1.1')
        ipam_config = docker.types.IPAMConfig(pool_configs=[ipam_pool])
        self.net = self.client.networks.create("network1", driver="bridge", ipam=ipam_config) 
        self.log.logger.info('Started docker network with name "network1" on subnet 173.20.1.0/24' )        
    
    def run_stress_tester(self, attack_type: str = '1', threads: int = 1000, url: str = '/policytypes', runtime: str = 30, policy: str = '') -> str:
        """
        Runs the external script 'Stress-tester'
        Parameters:
            attack_type: attack type according to the script, where 1 is a SYN flood and 3 is a HTTP flood
            threads: on how many threads the attack should be executed
        Returns:
            result: output of the attack
        """  
        print(url)
        result = subprocess.check_output(['./stress', self.s.a1_ip, self.s.a1_port, attack_type, str(threads), str(runtime), '30', url , self.s.policy_type, policy])   
        self.concurrent = False
        return result
    
    def command_in_container(self, container: docker.models.containers.Container = None, command: str = None) -> str:
        """
        Executes a command inside a docker container
        Parameters:
            container: the docker container
            command
        Returns:
            output: output of running the command    
        """
        output = container.exec_run(command)
        self.log.logger.info('Output from command in container: %s', output)
        return output
    
    def dos_check_second_http(self, container):
        """
        Continuously sends a http get request from a different IP (docker container)
        Parameters:
            container: the docker container  
        """
        while self.concurrent:
            max_execution_time = 15
            command = 'curl -v ' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes'

            with concurrent.futures.ThreadPoolExecutor() as executor:
                try:
                    future = executor.submit(self.command_in_container, container, command)
                    try:
                        exec_result = future.result(timeout=max_execution_time)
                        self.log.logger.info("GET request could be served normally from docker container during the HTTP attack.")
                    except concurrent.futures.TimeoutError:
                        self.log.logger.info("GET request could not be served in the defined time, which was %ss.", max_execution_time)
                except Exception as e:
                    self.log.logger.error(e)        
            sleep(1)
    
    def dos_check_second_tcp(self, container):
        """
        Continuously sends a tcp get request from a different IP (docker container)
        Parameters:
            container: the docker container  
        """
        while self.concurrent:
            max_execution_time = 15
            command = 'nc -v -z -w 15' + self.s.a1_ip + ' ' + self.s.a1_port
            with concurrent.futures.ThreadPoolExecutor() as executor:
                try:
                    future = executor.submit(self.command_in_container, container, command)
                    try:
                        exec_result = future.result(timeout=max_execution_time)
                        self.log.logger.info("SYN request could be served normally from docker container during the TCP attack.")
                    except concurrent.futures.TimeoutError:
                        self.log.logger.info("SYN request could not be served in the defined time, which was %ss.", max_execution_time)
                except Exception as e:
                    self.log.logger.error(e)        
            sleep(1)
            
    def stress_testing_tcp(self, container):
        """
        Starts the tcp flooding attack and evaluates the result
        Parameters:
            container: the docker container  
        """
        print('TCP connection flood, this might take some time...')
        self.log.logger.info('Start TCP connection flood')
        dos_unsuccessful = True
        threads = 5000
        while dos_unsuccessful:
            self.concurrent = True
            with concurrent.futures.ThreadPoolExecutor() as executor:
                result_1 = executor.submit(self.run_stress_tester, '1', threads)
                executor.submit(self.dos_check_second_tcp, container)
                
            output = result_1.result()
            ind_connection_error = str(output).find('Connection Error')
            ind_seperator = str(output).find('times', 218, 318)
            error_rate = str(output)[ind_connection_error+18:ind_seperator-1]
            error_rate_percentage = 100/threads * int(error_rate)
            self.log.logger.info('\n%s', ast.literal_eval(repr(output.decode('utf-8'))))
            try:
                if(error_rate_percentage > 10):
                    dos_unsuccessful = False
                    self.log.logger.info('Exhaustion was achieved with with %s threads.', str(threads))
                    self.log.logger.info('%s percent of the connections could not be established.', str(int(error_rate_percentage)))
                else:
                    self.log.logger.info('Attack not successful, trying with a higher thread number.')
                    threads = threads * 2       
            except Exception as e:
                self.log.logger.error('Stress testing did not work, check the connection. Error message: %s', e)   

    def stress_testing_http(self, container):
        """
        Starts the http flooding attack and evaluates the result
        Parameters:
            container: the docker container  
        """
        starting_url = self.get_starting_point()
        print('HTTP GET flood, this might take some time...')
        self.log.logger.info('Start HTTP connection flood')
        
        for threads in [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500]:
            for _ in range(5):
                runtime = 120
                while runtime <= 120:
                    self.concurrent = True
                    with concurrent.futures.ThreadPoolExecutor() as executor:
                        try:
                            result_1 = executor.submit(self.run_stress_tester, '2', threads, starting_url, runtime)
                        except Exception as e:
                            self.log.logger.info('Exception while running the stress tester %s', e)
                    output = result_1.result()
                    self.log.logger.info('\n%s', ast.literal_eval(repr(output.decode('utf-8')))) 
                    runtime += 30
                
    def stress_testing_policy(self, container):
        """
        Starts the http flooding attack and evaluates the result
        Parameters:
            container: the docker container  
        """
        print('HTTP PUT flood, this might take some time...')
        self.log.logger.info('Start HTTP connection flood')

        f = open(self.s.policy_path)
        data = str(f.read())
        
        for threads in [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 000, 9500]:
            for _ in range(5):
                runtime = 120
                while runtime <= 120:
                    with concurrent.futures.ThreadPoolExecutor() as executor:
                        try:
                            result_1 = executor.submit(self.run_stress_tester, '3', threads, '/', runtime, data)
                        except Exception as e:
                            self.log.logger.info('Exception while running the stress tester %s', e)    
                        # executor.submit(self.dos_check_second_http, container)
                    output = result_1.result()
                    self.log.logger.info('\n%s', ast.literal_eval(repr(output.decode('utf-8'))))    
                    runtime += 30         
    
    # def send_policy_loop(self, container):
    #     """
    #     Policy loop for each thread
    #     Parameters:
    #         container: docker container
    #     """
    #     f = open(self.s.policy_path)
    #     data = json.load(f)
    #     headers = {'Content-Type': 'application/json'}
    #     url_base = "http://" + self.s.a1_ip + ":" + self.s.a1_port + "/policytypes/" + self.s.policy_type +"/policies" 
    #     start_time = time()
    #     for sender in range(100):
    #         try:
    #             if self.s.tls_certificate:
    #                 response = requests.put(url_base + "/" + str(int(self.counter)+1), json=data, headers=headers, cert=self.s.tls_certificate)
    #             else:    
    #                 response = requests.put(url_base + "/" + str(int(self.counter)+1), json=data, headers=headers)
    #             self.counter += 1
    #             self.log.logger.info(response.text)  
    #         except Exception as e:
    #             self.log.logger.error('Exception when posting the policy. Error message: %s', e)
    #             return  
    #     end_time = time()
    #     print(end_time - start_time)
    
    # def policy_processing(self, container):
    #     """
    #     Starts the policy flooding attack and evaluates the result
    #     Parameters:
    #         container: docker container
    #     """
    #     print('Policy flood, this might take some time...')
    #     self.log.logger.info('Start Policy flood')
    #     threads = 1000
    #     self.concurrent = True
    #     with concurrent.futures.ThreadPoolExecutor() as executor:
    #         for sender in range(100):
    #             result = executor.submit(self.send_policy_loop, container)
       

    def health_check(self):
        """
        Checks if the server can be accessed after the attack 
        """
        url = 'http://' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes'
        for check in range(10):
            if self.s.tls_certificate:
                response = requests.get(url, cert=self.s.tls_certificate)
            else:    
                response = requests.get(url)
            if (response.status_code == 200):
                self.log.logger.info('Server successfully served a get request after the attack after %s seconds.', check)
                break
            else:
                self.log.logger.warning('Server did not recover from attack after %s seconds', check)   
            sleep(1)                             
