# Master Thesis - O-RAN A1 Interface Testing Tool



## Description
This testing tool was developed in the scope of a master thesis. It is built to test the implemented security measures of the A1 termination in the Near-RT RIC.

## Design Aspects:
The goals for designing our O-RAN Interface testing tool are listed below.

- **Interoperability:** The main goal is to achieve interoperability with different O-RAN setups. Interoperability was achieved by enabling the user to provide a parameter sheet that clearly defines the policy server setup within the limits of an O-RAN specification compliant implementation. This however comes with the drawback that not all possibilities of the specific setups are exploited, so it does not do setup specific health-checks but rather just checks the openness of the A1 interface.
- **Comprehensive user interface:** A comprehensible user interface is implemented by providing an easy setup when running a single script and various input rounds in the terminal to clearly define the test to perform.
- **Logging:** Clear output and a high level of correctness stem from a detailed and a specific logging to track various level of detail; To achieve a good overview and easy readability a summarized log is provided. To additionally dig deeper and not loose information an extensive log is created that gives insights in the exact messages that are sent and into potential issues.   

![Testing Tool Architecture](https://gitlab.ethz.ch/fluecks/master_thesis/-/raw/main/testing_tool_architecture.png)

**Workflow:**  If the defined preconditions are met, the tool can be deployed in the command line by typing 'python3.9 testing_interface.py' after cloning the github folder to the testing machine. The attack the tester wants to execute can be selected. After that a parameter.json document can be provided to specify the IP, port and URL entrypoint of the policy server, the deployed policy type and an example policy file. All this can also be provided directly in the command line. For the TLS and OAuth 2.0 test we only need the IP, port and URL entrypoint of the server. For all found policy types an example is provided in the 'example\_policies' folder. The test logs are stored in the testing tool folder in a directory with the starting time and date of the test. Every executed test is logged in its own file and the 'summary.log' file summarizes the major outcomes of the tests. 

**Programming language:** We developed our testing tool in Python which also interacts with secondary code written in bash and golang that partly got adapted.

**testssl.sh:** For testing the existing SSL/TLS setup the open source tool testssl.sh is integrated into the testing tool. Additionally to the simple protocol test the tool also allows testing for most of the well-known TLS vulnerabilities like Heartbleed or CCS injection. However, the simplicity of the policy server makes testing these vulnerabilities currently unnecessary.\\

**Stress-Tester:** For the DoS testing the open source tool Stress-tester is used as a backbone for sending TCP and HTTP messages. It was then largely customized to fit our specific security and performance requirements for measuring A1 policy throughput and response time. We made multiple fixes to Stress-tester's codebase to obtain accurate results.

## Features
This section describes all specific implemented features in the A1 Termination policy server testing tool of the Near-RT RIC.

### OAuth 2.0 Testing
The tool provides the functionality to check if the interface under test has OAuth 2.0 enforced by issuing a GET request and inspect the underlying authorization requirements. This is required as specified by the O-RAN Alliance in test case 15.4.3.STC-15-15.4-003 and security specification 5.1.3.2. It also checks for other authorization methods.

### DoS Testing
The tool supports multiple DoS tests the tester can choose from:

- **TCP SYN:** The TCP SYN DoS test opens as many connections at the same time to the A1 policy server as possible and outputs its limitation. Note: This requires the attack machine to be configured to open as many TCP connections as possible. The tester can define with how many concurrent open connections they want to run the attack and for how long the connection remains open. As a result it outputs how many connections can be opened and how many connection errors there are. At the end of the test a health-check of the connection is executed to ensure the accessibility of the server.
- **HTTP GET:** This enables the tool to flood the HTTP A1 policy server on the Near-RT RIC with GET request. The tester is able to define the specific URL they want to attack, for how long the flood should last and how many concurrent TCP connections should be opened at the same time. As a result the tool outputs the sent and received requests per second, the average time to respond to the GET requests, how many requests could not be returned during the given deadline and if there were any connection errors. At the end of the test a health-check of the connection is executed to ensure the accessibility of the server.
- **HTTP PUT:** With the HTTP PUT DoS test the tester can deploy a set of provided policies to the policy server. There is another level of nuance to this test, i.e., the policy can either be deployed to different URLs as 'new' policies or always to the same URL as 'updated' policies. The tester can specify the policy, the policy type, how long the test should run and how many concurrent TCP connections are to be used to send policies. As output, the sent and received requests per second, the average time to respond to the GET requests, how many requests could not be returned during the given deadline and if there were any connection errors are displayed. At the end of the test a health-check of the connection is executed to ensure the accessibility of the server.

The above DoS cases meet the security requirement REQ-SEC-NEAR-RT-6 of the O-RAN Alliance and is related to the DoS specified in the E2E test specifications, section 7.2.3.

### TLS Testing
The testing tool tests if the policy server enforces TLS 1.2 or 1.3 connections as specified by the O-RAN Alliance in the test case 15.4.1.STC-15-15.4-001 and security specification 5.1.3.2. 

### Policy Fuzzing
With the testing tool a simple policy fuzzing can be applied.  It checks if the policy is accepted or rejected depending on alterations of the policy contents. So all the keys of the provided JSON policy and all values get altered in multiple iterations. The policy server is expected to reject all policies with altered keys and all values that are not in the scope of the provided scheme. This test checks if the requirements in 5.2.4.3.1 in the A1 Application Protocol Specification from the O-RAN Alliance are respected.

### URL Fuzzing
The testing tool is capable of doing URL fuzzing. This searches for undeclared URLs that can be accessed in the policy server. The function was implemented to check if the server is built as specified by the O-RAN Alliance.

### Multiple Non-RT RICs
The testing tool provides a proof of concept of the multiple Non-RT RIC attack described in the thesis. It opens two containers with different IPs that act as different Non-RT RICs with a trusted connection to the policy server. One of these containers acts as a legitimate Non-RT RIC that deploys a policy with a PUT request. The other acts as a malicious Non-RT RIC that can look into all existing policies that were provided by the legitimate Non-RT RIC by issuing GET requests. It can then alter or delete the existing policy without the legitimate Non-RT RIC being informed about this change by sending a PUT or a DELETE request to the policy URL. The tester gets as an output if the attack succeeded or not.

## Preconditions
The testing tool is designed in a way that it should be applicable for all Near-RT RIC setups that have an architecture according to the O-RAN standards. So it relies on the specific setup of the server, which includes the URL tree and which URLs support which HTTP request methods. It also relies on the ORAN Alliance specified policy deployment. The A1 termination server in the Near-RT RIC must expose an HTTP service (IP and port) such that it can be reached by HTTP requests.

From a system-level perspective, the tool requires Ubuntu 18.04 OS, Python 3.9 and Docker 20.10.21. Other modules specified in the _requirements.txt_ have to be installed before running the testing tool. 
