from helper import SpecificLogger
import requests


class Authenticator:
    
    def __init__(self, session):
        self.s = session
        self.log = SpecificLogger('authorisation', session)
        
    def run(self): 
        response = self.test_authentication()
        self.authentication_type(response)
        self.log.summary()
            
    def test_authentication(self):
        """
        Sends a GET request to the server 
        """
        url = "http://" + self.s.a1_ip + ":" + self.s.a1_port + self.s.entrypoint + 'policytypes'
        if self.s.tls_certificate:
            response = requests.get(url, cert=self.s.tls_certificate)
        else:
            response = requests.get(url)
        self.log.logger.info('Response\n%s \n %s', response.headers,response.text)
        return response    
    
    def authentication_type(self, response: str = None): 
        """
        Evaluates the authentication requirements of the server
        Parameters:
            response: response of a GET request to the server 
        """
        reason = False
        print(response.headers)
        if response.status_code in [400, 401, 402, 403, 404, 405]:
            if response.reason == 'Unauthorized':
                reason = True
                self.log.logger.info('Response reason: Unauthorized') 
            if 'WWW-Authenticate' in response.headers:
                www_authenticate = response.headers['WWW-Authenticate']
                if 'OAuth2' in www_authenticate:
                    self.log.logger.info('OAuth2 is required')
                elif 'OAuth' in www_authenticate:
                    self.log.logger.info('OAuth is required')     
                else:
                    self.log.logger.info('OAuth not mentioned but other authorization, check the source code.')               
            for key in response.headers:
                if 'OAuth2' in response.headers[key] or 'OAuth2' in key:
                    self.log.logger.info('OAuth2 mentioned in response header')
                elif 'OAuth' in response.headers[key] or 'OAuth' in key:
                    self.log.logger.info('OAuth mentioned in response header')
            if not reason:
                self.log.logger.warning('Status code is: ', response.status_code, ' but authorization method is not mentioned in the response header.')
        elif response.status_code in [200,201,202,203,204,205]:
            self.log.logger.warning('Access without authorization possible.')    
        else:
            self.log.logger.warning('Response status code is:', response.status_code, ' with reason ', response.reason)               
            