from helper import SpecificLogger
import os
import subprocess


class TLSTester:
    
    def __init__(self, session):
        self.s = session
        self.log = SpecificLogger('tls', session)
        
    def run(self): 
        response = self.test_tls()
        self.log.summary()
        
    def get_user_choice(self) -> str:
        choice = input('\nEnter your choice (number): ')
        return choice    
        
    def show_tls_commands(self):
        print('1. Test which SSL/TLS protocols are implemented.')  
        print('2. Test Heartbleed vulnerability.')  
        print('3. Test CCS injection vulnerability.') 
        print('4. Test Ticketbleed vulnerability.')   
        print('5. Test ROBOT vulnerability.')  
        print('6. Test renegotiation vulnerability.')  
        print('7. Test CRIME vulnerability.')  
        print('8. Test BREACH vulnerability.')  
        print('9. Test POODLE vulnerability.')  
        print('10. Test TLS_FALLBACK_SCSV mitigation.')  
        print('11. Test SWEET32 vulnerability.')  
        print('12. Test BEAST vulnerability.')  
        print('13. Test LUCKY13 vulnerability.')  
        print('14. Test FREAK vulnerability.')  
        print('15. Test LOGJAM vulnerability.')  
        print('16. Test DROWN vulnerability.')  
        print('17. Test perfect forward secrecy settings.')  
        print('18. Check which RC4 diphers are being offered.')
        print('19. Test all mentioned vulnerabilities.')      
        
    def tls_test_translation(self, choice: str = None) -> str:
        if choice == '1':
            return '-p'
        elif choice == '2':
            return '-H'
        elif choice == '3':
            return '-I'
        elif choice == '4':
            return '-T'
        elif choice == '5':
            return '-BB'
        elif choice == '6':
            return '-R'
        elif choice == '7':
            return '-C'
        elif choice == '8':
            return '-B'
        elif choice == '9':
            return '-O'
        elif choice == '10':
            return '-Z'
        elif choice == '11':
            return '-W'
        elif choice == '12':
            return '-A'
        elif choice == '13':
            return '-L'
        elif choice == '14':
            return '-F'
        elif choice == '15':
            return '-J'
        elif choice == '16':
            return '-D'
        elif choice == '17':
            return '-f' 
        elif choice == '18':
            return '-4'
        elif choice == '19':
            return '-U'        
        
        
            
    def test_tls(self):
        """
        start ssl/tls testing tool
        """
        self.show_tls_commands()
        choice = self.get_user_choice()
        if os.path.exists('./testssl.sh/testssl.sh'):
            print('exists') 
        command = './testssl.sh/testssl.sh ' + self.tls_test_translation(choice) + ' https://' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes'     
        process = subprocess.run(command, shell=True)
        with process.stdout:
            self.log.logger.info('Output\n%s', process.stdout)
        exitcode = process.wait()      
                 
            