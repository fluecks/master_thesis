import requests
import os
import threading
import json

def send_policy_request(a1_ip: str = None, a1_port: str = None, policy: os.path = None, policytype: str = None) -> str:
    headers = {'Content-Type': 'application/json'}
    url_base = "http://" + a1_ip + ":" + a1_port + "/policytypes/" + policytype +"/policies"  
        
    f = open(policy)
    data = json.load(f)
    current_highest = list(map(int,requests.get(url_base).json()))
    current_highest_ind = max(current_highest)
        
    response = requests.put(url_base + "/" + str(int(current_highest_ind)+1), json=data, headers=headers)
    
        
    
def send_policy(a1_ip: str = None, a1_port: str = None, policy: os.path = None, policytype: str = None, multiple: int = 1, source_ip: str = None):
    for i in range(0,multiple):
        threading.Thread(target=send_policy_request(a1_ip, a1_port, policy, policytype)).start()    
    
    
def update_policy_request(source_ip: str = None, a1_ip: str = None, a1_port: str = None, policy: os.path = None, policytype: str = None):
    headers = {'Content-Type': 'application/json'}
    url = "http://" + a1_ip + ":" + a1_port + "/policytypes/" + policytype

    session = request.Session()

    requests.put(url, data=policy, headers=headers)
    
    session.close()
                
def update_policy(source_ip: str = None, a1_ip: str = None, a1_port: str = None, policy: os.path = None, multiple: int = 1, policytype: str = None):
    exist_check = request.get(url)
    if exist_check.status_code == 200:
        for i in range(1,multiple):
            threading.Thread(target=self.update_policy_request(source_ip, a1_ip, a1_port, policy, policytype)).start()
    else:
        print("Provided policy could not be updated since it does not exist.")        
            
def delete_policy_request(a1_ip: str = None, a1_port: str = None, policy: os.path = None, policytype: str = None):
    url = "http://" + a1_ip + ":" + a1_port + "/policytypes/" + policytype + "/policies"
    
    current_highest = list(map(int,requests.get(url).json()))
    if len(current_highest):
        current_highest_ind = max(current_highest)
        requests.delete(url + "/" + str(current_highest_ind)) 
    
    
def delete_policy(a1_ip: str = None, a1_port: str = None, policy: os.path = None, multiple: int = 1, policytype: str = None):
    
    for i in range(1,multiple):
        threading.Thread(target=delete_policy_request(a1_ip, a1_port, policy, policytype)).start()
  
    
def get_policy_types_request(a1_ip: str = None, a1_port: str = None):
    url = "http://" + a1_ip + ":" + a1_port + "/policytypes"
    
    session = request.Session()
        
    requests.delete(url)
    session.close()    

def get_policy_types(a1_ip: str = None, a1_port: str = None, multiple: int = 1):
    
    exist_check = request.get(url)
    
    if exist_check.status_code == 200:
        for i in range(1,multiple):
            threading.Thread(target=self.delete_policy_request(source_ip, a1_ip, a1_port, policy, policytype)).start()
    else:
        print("Provided url does not have the entrypoint policytypes.")  
        
def find_entrypoint(source_ip: str=None, a1_ip: str=None, a1_port: str=None):
    print("entrypoint")
    
def get_policy_status():
    print("status")
        
                          
                        