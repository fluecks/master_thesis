
from tools.dos import dos, rate_limiting

def run_ddos_check(a1_ip, a1_port):
    print("Starting DDoS Test...")
    tester = dos(a1_ip, a1_port)
    return tester.run()

def run_syn_rate_limiting(a1_ip, a1_port):
    print("Starting SYN flooding...")
    tester = rate_limiting(a1_ip, a1_port)
    return tester.run()


def main():
    print("starting main")
    run_ddos_check("192.168.84.11", "9639")
    #run_syn_rate_limiting("192.168.84.11", "9639")
    
if __name__ == "__main__":
    main()    