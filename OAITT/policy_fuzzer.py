import json
from helper import SpecificLogger, Session
import os
import random
import requests
import string
import subprocess

class PolicyFuzzer:
    
    def __init__(self, session: Session):
        self.s = session
        self.log = SpecificLogger('policy_fuzzing', session)
        
        random_seed = 12345
        self.min_fuzz_length = 1
        self.max_fuzz_length = 10
        self.fuzz_characters = string.ascii_letters + string.digits
        self.fuzz_int = string.digits
        random.seed(random_seed)
        
    def run(self):
        # policy fuzzing
        self.policy_altering_iterations() 
        # delete helper policy file
        subprocess.run(['rm', self.s.policy_path + '_copy.json'])
        self.log.summary()

    def read_policy(self):
        """
        Reads policy from json file.
        """
        with open(self.s.policy_path, 'r') as file:
            try: 
                data = json.load(file)
            except:
                self.log.logger.error('Policy could not be loaded from json file.')    
        return data
    
    def write_policy(self, data: dict = None):
        """
        Write policy to json file to use for post request
        Parameters:
            data: data that will be written to file
        """
        with open(self.s.policy_path + '_copy.json', 'w') as file:
            json.dump(data, file, indent=4)
    
       
    def modify_key(self, file: dict = None):
        """   
        Replaces a key in a dict with a fuzzed one
        Parameters:
            file: dict where keys should be replaced
        """ 
        keys_to_modify = []
        for key in file.keys():
            if key != 'ueId':
                if isinstance(file[key], dict):
                    self.modify_key(file[key])
                elif isinstance(file[key], list):
                    for i in file[key]:
                        self.modify_key(i) 
                keys_to_modify.append(key)   
        
        for key in keys_to_modify:   
            replace = random.choice([True, False])     
            if replace: 
                fuzz_length = random.randint(self.min_fuzz_length, self.max_fuzz_length)
                fuzz_string = ''.join(random.choice(self.fuzz_characters) for _ in range(fuzz_length))
                value = file.pop(key)
                file[fuzz_string] = value
                
                
    def modify_single_key(self, file: dict = None, key: str = None):
        """   
        Replaces a single key in a dict with a fuzzed one
        Parameters:
            file: dict where keys should be replaced
        """ 
        key_to_modify = key
        if key != 'ueId':
                fuzz_length = random.randint(self.min_fuzz_length, self.max_fuzz_length)
                fuzz_string = ''.join(random.choice(self.fuzz_characters) for _ in range(fuzz_length))
                value = file.pop(key_to_modify)
                file[fuzz_string] = value            
       
              
    def modify_value(self, file: dict = None): 
        """   
        Replaces a value in a dict with a fuzzed one
        Parameters:
            file: dict where values should be replaced
        Returns:
            file: altered dict    
        """ 
        for key, value in file.items():
            if key != 'ueId':
                if isinstance(value, dict):
                    file[key] = self.modify_value(value) 
                elif isinstance(value, list):
                    for index, listvalue in enumerate(value):
                        if isinstance(listvalue, dict):
                            file[key][index] = self.modify_value(listvalue)  
                                
                if isinstance(value, int):
                    fuzz_length = random.randint(self.min_fuzz_length, self.max_fuzz_length)
                    fuzz_string = ''.join(random.choice(self.fuzz_int) for _ in range(fuzz_length))
                    file[key] = int(fuzz_string)
                if isinstance(value, str):
                    fuzz_length = random.randint(self.min_fuzz_length, self.max_fuzz_length)
                    fuzz_string = ''.join(random.choice(self.fuzz_characters) for _ in range(fuzz_length))
                    file[key] = fuzz_string                      
        return file    
    
    def modify_single_value(self, file: dict = None, key: str = None): 
        """   
        Replaces a single value in a dict with a fuzzed one
        Parameters:
            file: dict where values should be replaced
        Returns:
            file: altered dict    
        """ 
        value = file[key]
        if key != 'ueId':           
            if isinstance(value, int):
                fuzz_length = random.randint(self.min_fuzz_length, self.max_fuzz_length)
                fuzz_string = ''.join(random.choice(self.fuzz_int) for _ in range(fuzz_length))
                file[key] = int(fuzz_string)
            if isinstance(value, str):
                fuzz_length = random.randint(self.min_fuzz_length, self.max_fuzz_length)
                fuzz_string = ''.join(random.choice(self.fuzz_characters) for _ in range(fuzz_length))
                file[key] = fuzz_string                      
        return file                                     

    def post_to_url(self, url: str = None) -> requests.Response:
        """
        Post the altered policy to the URL
        Parameters:
            url: URL to which policy is posted
        Returns:
            response: response to post request      
        """  
        headers = {'Content-Type': 'application/json'}
        if self.s.tls_certificate:
            return requests.put(url, data=self.s.policy_path + '_copy.json', headers=headers, cert=self.s.tls_certificate)
        else:
            return requests.put(url, data=self.s.policy_path + '_copy.json', headers=headers)        
    
    def evaluation_policy(self, response: requests.Response = None) -> bool:
        """
        Analyses the response of the POST request
        Parameters:
            response: response of a POST request
        Returns:
            bool: indicates if the request was succesful    
        """
        if response.status_code in [400, 401, 402, 403, 404]:
            self.log.logger.error('Posting did not work, there was something wrong with the connection.')
        elif 'error' in response.text:
            self.log.logger.info('Posting worked, but the policy was not accepted by the server.')  
        else:
            self.log.logger.info('The fuzzed policy file was accepted by the server')   
            return True   
        return False
                
    def policy_altering_iterations(self):
        """
        Policy fuzzing on the base of the provided policy
        """
        url = 'http://' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + 'policytypes/' + self.s.policy_type + '/policies/1' 
        if os.path.exists(self.s.policy_path):
            json_data = self.read_policy()
        else:    
            print('Given policy path does not exist.')
            
        num_iterations = 1000
        
        # modify single keys
        altered_data = json_data
        self.log.logger.info("---- Modify Single Keys ----")
        for key in altered_data.keys():
            policy = self.read_policy()
            self.modify_single_key(policy, key)
            self.write_policy(policy)    
            response = self.post_to_url(url) 
            accepted = self.evaluation_policy(response)
            if accepted:
                self.log.logger.warning('Accepted data: %s', policy)
            else: 
                self.log.logger.info('Rejected data: %s', policy)  
            
        # modify multiple keys
        altered_data = self.read_policy()
        self.log.logger.info("---- Modify Keys ----")
        for i in range(num_iterations):
            self.modify_key(altered_data)
            self.write_policy(altered_data)    
            response = self.post_to_url(url) 
            accepted = self.evaluation_policy(response)
            if accepted:
                self.log.logger.warning('Accepted data: %s', altered_data)
            else: 
                self.log.logger.info('Rejected data: %s', altered_data)    
        
        # modify single values
        altered_data = self.read_policy()
        self.log.logger.info("---- Modify Single Values ----")
        for key in altered_data.keys():
            policy = self.read_policy()
            self.modify_single_value(policy, key)
            self.write_policy(policy)    
            response = self.post_to_url(url) 
            accepted = self.evaluation_policy(response) 
            if accepted:
                self.log.logger.warning('Accepted data: %s', altered_data) 
            else:
                self.log.logger.info('Rejected data: %s', altered_data)  
                
        # modify only values
        altered_data = self.read_policy()
        self.log.logger.info("---- Modify Values ----")
        for i in range(num_iterations):
            self.modify_value(altered_data)
            self.write_policy(altered_data)    
            response = self.post_to_url(url) 
            accepted = self.evaluation_policy(response) 
            if accepted:
                self.log.logger.warning('Accepted data: %s', altered_data) 
            else:
                self.log.logger.info('Rejected data: %s', altered_data)      
        
        # combination
        altered_data = self.read_policy()
        self.log.logger.info("---- Modify Keys and Values ----")
        for i in range(num_iterations):  
            self.modify_key(altered_data)
            altered_data = self.modify_value(altered_data)
            self.write_policy(altered_data)    
            response = self.post_to_url(url)
            accepted = self.evaluation_policy(response) 
            if accepted:
                self.log.logger.warning('Accepted data: %s', altered_data)
            else:
                self.log.logger.info('Rejected data: %s', altered_data)      