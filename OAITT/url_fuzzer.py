from helper import SpecificLogger, Session
import random
import requests
import string

class UrlFuzzer:
    
    def __init__(self, session: Session):
        self.s = session
        self.log = SpecificLogger('url_fuzzing', session)
        
        random_seed = 12345
        self.min_fuzz_length = 1
        self.max_fuzz_length = 10
        self.fuzz_characters = string.ascii_letters + string.digits
        self.fuzz_int = string.digits
        random.seed(random_seed)
        
    def run(self):
        # url fuzzing
        self.url_altering_iterations()  
        self.log.summary()
       
    def get_url(self, url: str = None):
        """
        Get the content of the URL
        Parameters:
            url: URL to get
        Returns:
            response: response to GET request      
        """
        if self.s.tls_certificate:
            return requests.get(url, cert=self.s.tls_certificate)
        else:
            return requests.get(url)   
    
    def evaluation_url(self, response: requests.Response = None) -> bool:
        """
        Analyses the response of the GET request
        Parameters:
            response: response of a GET request
        Returns:
            bool: indicates if the request was succesful    
        """
        if response.status_code in [400, 401, 402, 403, 404]:
            return False
        else:
            self.log.logger.warning('URL could be accessed: %s', response)
            return True   
                            
    def url_altering_iterations(self):
        """
        URL fuzzing on the base of the provided entrypoint   
        """
        num_iterations = 1000 
        
        for entrypoint in ['', 'policytypes/']:
            target_url = 'http://' + self.s.a1_ip + ':' + self.s.a1_port + self.s.entrypoint + entrypoint
            self.log.logger.info("---- Fuzz URL with base %s ----", target_url)
            for i in range(num_iterations):
                # Generate a random fuzz string
                fuzz_length = random.randint(self.min_fuzz_length, self.max_fuzz_length)
                fuzz_string = ''.join(random.choice(self.fuzz_characters) for _ in range(fuzz_length))
            
                # Construct the fuzzed values
                fuzzed_url = target_url + fuzz_string
                response = self.get_url(fuzzed_url)
                accessed = self.evaluation_url(response)
                if accessed:
                    self.log.logger.info('Accessed URL: %s', fuzzed_url)
                else:
                    self.log.logger.info('Rejected URL: %s', fuzzed_url)    